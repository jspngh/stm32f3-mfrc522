# Example project for mfrc522 + stm32f3xx-hal
An example project for the [mfrc522 device driver crate](https://gitlab.com/jspngh/rfid-rs)
using the [stm32f3xx-hal](https://github.com/stm32-rs/stm32f3xx-hal).

Please note that this specific example was added upon request,
and is not tested due to lack of the required hardware.
